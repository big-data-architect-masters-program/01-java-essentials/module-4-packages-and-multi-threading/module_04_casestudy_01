public class Main {

    public static void main(String[] args) {
        Thread1 thread1 = new Thread1();
        thread1.start();
        Thread2 thread2 = new Thread2();
        thread2.start();

    }
}

class Thread1 extends Thread {
    @Override
    public void run() {
        for (int i = 0; i <= 10; i++) {
            System.out.println("thread1:" + i);
        }
    }
}

class Thread2 extends Thread {
    @Override
    public void run() {
        for (int i = 101; i <= 110; i++) {
            System.out.println("thread2:" + i);
        }
    }
}